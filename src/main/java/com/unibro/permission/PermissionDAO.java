/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.permission;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface PermissionDAO {

    public void setDataSource(DataSource ds);

    public Permission createObject(Permission object);

    public Permission getObject(String id);

    public Permission updateObject(Permission object);

    public Integer deleteObject(Permission object);

    public List<Permission> getAllObjects();

    public List<Permission> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Permission queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Permission> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
    
    public List<Permission> loadPermissionByGroupId(int groupid);
    
    public List<Permission> loadPermissionByFunctionId(int functionid);
}
