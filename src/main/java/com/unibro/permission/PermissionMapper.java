/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.permission;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class PermissionMapper implements RowMapper<Permission> {

    @Override
    public Permission mapRow(ResultSet rs, int rowNum) throws SQLException {
        Permission object = new Permission();
        object.setFunc_id((Integer) rs.getObject("func_id"));
        object.setGroup_id((Integer) rs.getObject("group_id"));
        object.setOpen((Boolean) rs.getObject("permission.open"));
        object.setView((Boolean) rs.getObject("permission.view"));
        object.setAdd((Boolean) rs.getObject("permission.add"));
        object.setEdit((Boolean) rs.getObject("permission.edit"));
        object.setDelete((Boolean) rs.getObject("permission.delete"));
        object.setPrint((Boolean) rs.getObject("permission.print"));
        object.setFind((Boolean) rs.getObject("permission.find"));
        object.setCalculate((Boolean) rs.getObject("permission.calculate"));
        object.setLock((Boolean) rs.getObject("permission.lock"));
        object.setFull((Boolean) rs.getObject("permission.full"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
