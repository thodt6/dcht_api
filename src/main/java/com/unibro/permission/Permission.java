package com.unibro.permission;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Permission implements Serializable {

    private Integer func_id = 0;
    private Integer group_id = 0;
    private Boolean open = Boolean.FALSE;
    private Boolean view = Boolean.FALSE;
    private Boolean add = Boolean.FALSE;
    private Boolean edit = Boolean.FALSE;
    private Boolean delete = Boolean.FALSE;
    private Boolean print = Boolean.FALSE;
    private Boolean find = Boolean.FALSE;
    private Boolean calculate = Boolean.FALSE;
    private Boolean lock = Boolean.FALSE;
    private Boolean full = Boolean.FALSE;
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_time = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Permission.class);

    public void setFunc_id(Integer func_id) {
        this.func_id = func_id;
    }

    @JsonProperty("func_id")
    public Integer getFunc_id() {
        return this.func_id;
    }

    public void setGroup_id(Integer group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("group_id")
    public Integer getGroup_id() {
        return this.group_id;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    @JsonProperty("open")
    public Boolean getOpen() {
        return this.open;
    }

    public void setView(Boolean view) {
        this.view = view;
    }

    @JsonProperty("view")
    public Boolean getView() {
        return this.view;
    }

    public void setAdd(Boolean add) {
        this.add = add;
    }

    @JsonProperty("add")
    public Boolean getAdd() {
        return this.add;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    @JsonProperty("edit")
    public Boolean getEdit() {
        return this.edit;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    @JsonProperty("delete")
    public Boolean getDelete() {
        return this.delete;
    }

    public void setPrint(Boolean print) {
        this.print = print;
    }

    @JsonProperty("print")
    public Boolean getPrint() {
        return this.print;
    }

    public void setFind(Boolean find) {
        this.find = find;
    }

    @JsonProperty("find")
    public Boolean getFind() {
        return this.find;
    }

    public void setCalculate(Boolean calculate) {
        this.calculate = calculate;
    }

    @JsonProperty("calculate")
    public Boolean getCalculate() {
        return this.calculate;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    @JsonProperty("lock")
    public Boolean getLock() {
        return this.lock;
    }

    public void setFull(Boolean full) {
        this.full = full;
    }

    @JsonProperty("full")
    public Boolean getFull() {
        return this.full;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_time(java.util.Date create_time) {
        this.create_time = create_time;
    }

    @JsonProperty("create_time")
    public java.util.Date getCreate_time() {
        return this.create_time;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public static Permission getInstant() {
        Permission instant = new Permission();
        instant.setFunc_id(0);
        instant.setGroup_id(0);
        instant.setOpen(Boolean.FALSE);
        instant.setView(Boolean.FALSE);
        instant.setAdd(Boolean.FALSE);
        instant.setEdit(Boolean.FALSE);
        instant.setDelete(Boolean.FALSE);
        instant.setPrint(Boolean.FALSE);
        instant.setFind(Boolean.FALSE);
        instant.setCalculate(Boolean.FALSE);
        instant.setLock(Boolean.FALSE);
        instant.setFull(Boolean.FALSE);
        instant.setCreate_user(0);
        instant.setCreate_time(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);

        return instant;
    }

    public Permission() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Permission)) {
            return false;
        }
        Permission compareObj = (Permission) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("func_id", this.func_id);
        map.put("group_id", this.group_id);
        map.put("open", this.open);
        map.put("view", this.view);
        map.put("add", this.add);
        map.put("edit", this.edit);
        map.put("delete", this.delete);
        map.put("print", this.print);
        map.put("find", this.find);
        map.put("calculate", this.calculate);
        map.put("lock", this.lock);
        map.put("full", this.full);
        map.put("create_user", this.create_user);
        map.put("create_time", this.create_time);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);

        return map;
    }

    public String getUniqueKey() {
        return func_id.toString() + "_" + group_id.toString();
    }

}
