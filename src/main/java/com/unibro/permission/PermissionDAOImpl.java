/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.permission;

import com.unibro.utils.RequestFilter;
import com.unibro.utils.Global;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author THOND
 */
public class PermissionDAOImpl implements PermissionDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(PermissionDAOImpl.class);
    private String exceptionMsg = "";
    private HttpStatus exceptionCode = HttpStatus.OK;
    private String exceptionType = "";

    private static final String columns = "func_id,group_id,permission.open,permission.view,permission.add,permission.edit,permission.delete,permission.print,permission.find,permission.calculate,permission.lock,permission.full,create_user,create_time,update_user,update_time,version,delete_flag";
    private static final String columns_key = "func_id,group_id";

    @Autowired
    private static Environment env;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Permission createObject(final Permission object) {
        try {
            int size = columns.split(",").length;
            final String SQL = "INSERT INTO " + Global.getConfigValue("app.dbname") + ".permission(" + columns + ") VALUES (" + Global.getNumberCharater(size, "?") + ")";;
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, columns.split(","));
                    ps.setObject(1, object.getFunc_id());
                    ps.setObject(2, object.getGroup_id());
                    ps.setObject(3, object.getOpen());
                    ps.setObject(4, object.getView());
                    ps.setObject(5, object.getAdd());
                    ps.setObject(6, object.getEdit());
                    ps.setObject(7, object.getDelete());
                    ps.setObject(8, object.getPrint());
                    ps.setObject(9, object.getFind());
                    ps.setObject(10, object.getCalculate());
                    ps.setObject(11, object.getLock());
                    ps.setObject(12, object.getFull());
                    ps.setObject(13, object.getCreate_user());
                    ps.setObject(14, object.getCreate_time());
                    ps.setObject(15, object.getUpdate_user());
                    ps.setObject(16, object.getUpdate_time());
                    ps.setObject(17, object.getVersion());
                    ps.setObject(18, object.getDelete_flag());

                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is created";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Permission getObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Integer v1 = Integer.valueOf(keys_value[1]);
            Object[] values = new Object[]{v0, v1};

            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".permission WHERE " + Global.getMaskSQL(columns_key, "AND");
            List<Permission> list = this.jdbcTemplateObject.query(SQL, values, new PermissionMapper());
            if (list == null || list.isEmpty()) {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            } else {
                if (list.size() == 1) {
                    this.exceptionCode = HttpStatus.OK;
                    this.exceptionMsg = "";
                    return list.get(0);
                } else {
                    this.exceptionCode = HttpStatus.NOT_FOUND;
                    this.exceptionMsg = "IncorrectResultSizeDataAccessException";
                    return null;
                }
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Permission updateObject(final Permission object) {
        try {
            String[] prepared_arr = (columns + "," + columns_key).split(",");
            final String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".permission SET " + Global.getMaskSQL(columns, ",") + " WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, prepared_arr);
                    ps.setObject(1, object.getFunc_id());
                    ps.setObject(2, object.getGroup_id());
                    ps.setObject(3, object.getOpen());
                    ps.setObject(4, object.getView());
                    ps.setObject(5, object.getAdd());
                    ps.setObject(6, object.getEdit());
                    ps.setObject(7, object.getDelete());
                    ps.setObject(8, object.getPrint());
                    ps.setObject(9, object.getFind());
                    ps.setObject(10, object.getCalculate());
                    ps.setObject(11, object.getLock());
                    ps.setObject(12, object.getFull());
                    ps.setObject(13, object.getCreate_user());
                    ps.setObject(14, object.getCreate_time());
                    ps.setObject(15, object.getUpdate_user());
                    ps.setObject(16, object.getUpdate_time());
                    ps.setObject(17, object.getVersion());
                    ps.setObject(18, object.getDelete_flag());
// Primare key
                    ps.setObject(19, object.getFunc_id());
                    ps.setObject(20, object.getGroup_id());

                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is updated";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Integer deleteObject(Permission object) {
        try {
            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".permission WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, object.getFunc_id(), object.getGroup_id());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer deleteObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Integer v1 = Integer.valueOf(keys_value[1]);
            Object[] values = new Object[]{v0, v1};

            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".permission where " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, values);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public List<Permission> getAllObjects() {
        try {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".permission";
            List<Permission> objects = jdbcTemplateObject.query(SQL, new PermissionMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public List<Permission> loadObjects(String SQL, Object... params) {
        try {
            List<Permission> objects = jdbcTemplateObject.query(SQL, params, new PermissionMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Permission queryForObject(String SQL, Object... params) {
        try {
            Permission ret = this.jdbcTemplateObject.queryForObject(SQL, params, new PermissionMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Integer getRowCount(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer getTotalRow() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".permission", Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Boolean checkObjectExist(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Integer v1 = Integer.valueOf(keys_value[1]);
            Object[] values = new Object[]{v0, v1};

            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".permission WHERE " + Global.getMaskSQL(columns_key, "AND"), values, Integer.class) > 0;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return false;
        }
    }

    @Override
    public List<Permission> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".permission";
            //No sort
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            return this.loadObjects(SQL);
        } else {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".permission WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <=? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            logger.info("Filter SQL:" + SQL);
            return this.loadObjects(SQL, condition.toArray());
        }

    }

    @Override
    public int filterObjectsSize(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalRow();
        } else {
            String SQL = "SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".permission WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getRowCount(SQL, condition.toArray());
        }

    }

    /**
     * @return the exceptionMsg
     */
    public String getExceptionMsg() {
        return exceptionMsg;
    }

    /**
     * @param exceptionMsg the exceptionMsg to set
     */
    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    /**
     * @return the exceptionCode
     */
    public HttpStatus getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(HttpStatus exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    /**
     * @return the exceptionType
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * @param exceptionType the exceptionType to set
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    @Override
    public List<Permission> loadPermissionByGroupId(int groupid) {
        String SQL = "SELECT dcht.permission.* FROM dcht.permission WHERE permission.group_id=?";
        return this.loadObjects(SQL, groupid);
    }

    @Override
    public List<Permission> loadPermissionByFunctionId(int functionid) {
        String SQL = "SELECT dcht.permission.* FROM dcht.permission WHERE permission.func_id=?";
        return this.loadObjects(SQL, functionid);
    }

}
