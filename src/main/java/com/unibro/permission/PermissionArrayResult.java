/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.permission;

import java.util.List;

/**
 *
 * @author THOND
 */
public class PermissionArrayResult {

    private List<Permission> data;
    private Integer size;

    public PermissionArrayResult(List<Permission> data, Integer size) {
        this.data = data;
        this.size = size;
    }

    /**
     * @return the data
     */
    public List<Permission> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Permission> data) {
        this.data = data;
    }

    /**
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
