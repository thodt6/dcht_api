/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.district;

import com.unibro.province.Province;
import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface DistrictDAO {

    public void setDataSource(DataSource ds);

    public District createObject(District object);

    public District getObject(String id);

    public District updateObject(District object);

    public Integer deleteObject(District object);

    public List<District> getAllObjects();

    public List<District> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public District queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<District> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);

    public District getByName(String name,int province_id);    
    
    public Integer updateDistrict(int current_id, int update_id);    
    

}
