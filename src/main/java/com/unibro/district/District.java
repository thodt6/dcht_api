package com.unibro.district;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class District implements Serializable {

    private Integer district_id = 0;
    private String district_code = "";
    private String name = "";
    private Integer population = 0;
    private Integer province_id = 0;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(District.class);

    public void setDistrict_id(Integer district_id) {
        this.district_id = district_id;
    }

    @JsonProperty("district_id")
    public Integer getDistrict_id() {
        return this.district_id;
    }

    public void setDistrict_code(String district_code) {
        this.district_code = district_code;
    }

    @JsonProperty("district_code")
    public String getDistrict_code() {
        return this.district_code;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    @JsonProperty("population")
    public Integer getPopulation() {
        return this.population;
    }

    public void setProvince_id(Integer province_id) {
        this.province_id = province_id;
    }

    @JsonProperty("province_id")
    public Integer getProvince_id() {
        return this.province_id;
    }

    public static District getInstant() {
        District instant = new District();
        instant.setDistrict_id(0);
        instant.setDistrict_code("");
        instant.setName("");
        instant.setPopulation(0);
        instant.setProvince_id(0);

        return instant;
    }

    public District() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof District)) {
            return false;
        }
        District compareObj = (District) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("district_id", this.district_id);
        map.put("district_code", this.district_code);
        map.put("name", this.name);
        map.put("population", this.population);
        map.put("province_id", this.province_id);

        return map;
    }

    public String getUniqueKey() {
        return district_id.toString();
    }

}
