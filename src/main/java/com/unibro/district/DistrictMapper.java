/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.district;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class DistrictMapper implements RowMapper<District> {

    @Override
    public District mapRow(ResultSet rs, int rowNum) throws SQLException {
        District object = new District();
        object.setDistrict_id((Integer) rs.getObject("district_id"));
        object.setDistrict_code((String) rs.getObject("district_code"));
        object.setName((String) rs.getObject("name"));
        object.setPopulation((Integer) rs.getObject("population"));
        object.setProvince_id((Integer) rs.getObject("province_id"));

        return object;
    }
}
