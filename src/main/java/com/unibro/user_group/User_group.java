package com.unibro.user_group;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class User_group implements Serializable {

    private Integer userid = 0;
    private Integer groupid = 0;
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_time = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(User_group.class);

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @JsonProperty("userid")
    public Integer getUserid() {
        return this.userid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    @JsonProperty("groupid")
    public Integer getGroupid() {
        return this.groupid;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_time(java.util.Date create_time) {
        this.create_time = create_time;
    }

    @JsonProperty("create_time")
    public java.util.Date getCreate_time() {
        return this.create_time;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public static User_group getInstant() {
        User_group instant = new User_group();
        instant.setUserid(0);
        instant.setGroupid(0);
        instant.setCreate_user(0);
        instant.setCreate_time(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);

        return instant;
    }

    public User_group() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User_group)) {
            return false;
        }
        User_group compareObj = (User_group) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("userid", this.userid);
        map.put("groupid", this.groupid);
        map.put("create_user", this.create_user);
        map.put("create_time", this.create_time);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);

        return map;
    }

    public String getUniqueKey() {
        return userid.toString() + "_" + groupid.toString();
    }

}
