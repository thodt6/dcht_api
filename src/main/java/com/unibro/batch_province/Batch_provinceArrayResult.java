/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_province;

import java.util.List;

/**
 *
 * @author THOND
 */
public class Batch_provinceArrayResult {

    private List<Batch_province> data;
    private Integer size;

    public Batch_provinceArrayResult(List<Batch_province> data, Integer size) {
        this.data = data;
        this.size = size;
    }

    /**
     * @return the data
     */
    public List<Batch_province> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Batch_province> data) {
        this.data = data;
    }

    /**
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
