/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_province;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class Batch_provinceMapper implements RowMapper<Batch_province> {

    @Override
    public Batch_province mapRow(ResultSet rs, int rowNum) throws SQLException {
        Batch_province object = new Batch_province();
        object.setProvince_id((Integer) rs.getObject("province_id"));
        object.setBatch_id((Integer) rs.getObject("batch_id"));
        object.setCp((Integer) rs.getObject("cp"));
        object.setCm((Integer) rs.getObject("cm"));
        object.setBv((Integer) rs.getObject("bv"));
        object.setInactive_cp((Integer) rs.getObject("inactive_cp"));
        object.setInactive_cm((Integer) rs.getObject("inactive_cm"));
        object.setInactive_bv((Integer) rs.getObject("inactive_bv"));
        object.setTotal_dead((Integer) rs.getObject("total_dead"));
        object.setTotal_live((Integer) rs.getObject("total_live"));
        object.setTotal_cost_dead((Long) rs.getObject("total_cost_dead"));
        object.setTotal_cost_live((Long) rs.getObject("total_cost_live"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
