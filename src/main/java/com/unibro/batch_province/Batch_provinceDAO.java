/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_province;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface Batch_provinceDAO {

    public void setDataSource(DataSource ds);

    public Batch_province createObject(Batch_province object);

    public Batch_province getObject(String id);

    public Batch_province updateObject(Batch_province object);

    public Integer deleteObject(Batch_province object);

    public List<Batch_province> getAllObjects();

    public List<Batch_province> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Batch_province queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Batch_province> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
    
    public int getTotalObject(int state,List<RequestFilter> filters);
    
    public long getTotalCost(int state,List<RequestFilter> filters);
}
