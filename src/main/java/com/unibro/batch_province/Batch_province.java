package com.unibro.batch_province;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigInteger;

public class Batch_province implements Serializable {

    private Integer province_id = 0;
    private Integer batch_id = 0;
    private Integer cp = 0;
    private Integer cm = 0;
    private Integer bv = 0;
    private Integer inactive_cp = 0;
    private Integer inactive_cm = 0;
    private Integer inactive_bv = 0;
    private Integer total_dead = 0;
    private Integer total_live = 0;
    private Long total_cost_dead = System.currentTimeMillis();
    private Long total_cost_live = System.currentTimeMillis();
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_time = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Batch_province.class);

    public void setProvince_id(Integer province_id) {
        this.province_id = province_id;
    }

    @JsonProperty("province_id")
    public Integer getProvince_id() {
        return this.province_id;
    }

    public void setBatch_id(Integer batch_id) {
        this.batch_id = batch_id;
    }

    @JsonProperty("batch_id")
    public Integer getBatch_id() {
        return this.batch_id;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    @JsonProperty("cp")
    public Integer getCp() {
        return this.cp;
    }

    public void setCm(Integer cm) {
        this.cm = cm;
    }

    @JsonProperty("cm")
    public Integer getCm() {
        return this.cm;
    }

    public void setBv(Integer bv) {
        this.bv = bv;
    }

    @JsonProperty("bv")
    public Integer getBv() {
        return this.bv;
    }

    public void setInactive_cp(Integer inactive_cp) {
        this.inactive_cp = inactive_cp;
    }

    @JsonProperty("inactive_cp")
    public Integer getInactive_cp() {
        return this.inactive_cp;
    }

    public void setInactive_cm(Integer inactive_cm) {
        this.inactive_cm = inactive_cm;
    }

    @JsonProperty("inactive_cm")
    public Integer getInactive_cm() {
        return this.inactive_cm;
    }

    public void setInactive_bv(Integer inactive_bv) {
        this.inactive_bv = inactive_bv;
    }

    @JsonProperty("inactive_bv")
    public Integer getInactive_bv() {
        return this.inactive_bv;
    }

    public void setTotal_dead(Integer total_dead) {
        this.total_dead = total_dead;
    }

    @JsonProperty("total_dead")
    public Integer getTotal_dead() {
        return this.total_dead;
    }

    public void setTotal_live(Integer total_live) {
        this.total_live = total_live;
    }

    @JsonProperty("total_live")
    public Integer getTotal_live() {
        return this.total_live;
    }

    public void setTotal_cost_dead(Long total_cost_dead) {
        this.total_cost_dead = total_cost_dead;
    }

    @JsonProperty("total_cost_dead")
    public Long getTotal_cost_dead() {
        return this.total_cost_dead;
    }

    public void setTotal_cost_live(Long total_cost_live) {
        this.total_cost_live = total_cost_live;
    }

    @JsonProperty("total_cost_live")
    public Long getTotal_cost_live() {
        return this.total_cost_live;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_time(java.util.Date create_time) {
        this.create_time = create_time;
    }

    @JsonProperty("create_time")
    public java.util.Date getCreate_time() {
        return this.create_time;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public static Batch_province getInstant() {
        Batch_province instant = new Batch_province();
        instant.setProvince_id(0);
        instant.setBatch_id(0);
        instant.setCp(0);
        instant.setCm(0);
        instant.setBv(0);
        instant.setInactive_cp(0);
        instant.setInactive_cm(0);
        instant.setInactive_bv(0);
        instant.setTotal_dead(0);
        instant.setTotal_live(0);
        instant.setTotal_cost_dead(System.currentTimeMillis());
        instant.setTotal_cost_live(System.currentTimeMillis());
        instant.setCreate_user(0);
        instant.setCreate_time(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);

        return instant;
    }

    public Batch_province() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Batch_province)) {
            return false;
        }
        Batch_province compareObj = (Batch_province) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("province_id", this.province_id);
        map.put("batch_id", this.batch_id);
        map.put("cp", this.cp);
        map.put("cm", this.cm);
        map.put("bv", this.bv);
        map.put("inactive_cp", this.inactive_cp);
        map.put("inactive_cm", this.inactive_cm);
        map.put("inactive_bv", this.inactive_bv);
        map.put("total_dead", this.total_dead);
        map.put("total_live", this.total_live);
        map.put("total_cost_dead", this.total_cost_dead);
        map.put("total_cost_live", this.total_cost_live);
        map.put("create_user", this.create_user);
        map.put("create_time", this.create_time);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);

        return map;
    }

    public String getUniqueKey() {
        return province_id.toString() + "_" + batch_id.toString();
    }

}
