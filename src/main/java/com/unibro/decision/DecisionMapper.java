/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.decision;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class DecisionMapper implements RowMapper<Decision> {

    @Override
    public Decision mapRow(ResultSet rs, int rowNum) throws SQLException {
        Decision object = new Decision();
        object.setId((Integer) rs.getObject("id"));
        object.setName((String) rs.getObject("name"));
        object.setDescription((String) rs.getObject("description"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
