/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.decision;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface DecisionDAO {

    public void setDataSource(DataSource ds);

    public Decision createObject(Decision object);

    public Decision getObject(String id);

    public Decision updateObject(Decision object);

    public Integer deleteObject(Decision object);

    public List<Decision> getAllObjects();

    public List<Decision> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Decision queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Decision> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
}
