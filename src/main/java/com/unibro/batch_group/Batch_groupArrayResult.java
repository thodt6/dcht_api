/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_group;

import java.util.List;

/**
 *
 * @author THOND
 */
public class Batch_groupArrayResult {

    private List<Batch_group> data;
    private Integer size;

    public Batch_groupArrayResult(List<Batch_group> data, Integer size) {
        this.data = data;
        this.size = size;
    }

    /**
     * @return the data
     */
    public List<Batch_group> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Batch_group> data) {
        this.data = data;
    }

    /**
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
