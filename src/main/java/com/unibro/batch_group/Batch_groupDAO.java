/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_group;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface Batch_groupDAO {

    public void setDataSource(DataSource ds);

    public Batch_group createObject(Batch_group object);

    public Batch_group getObject(String id);

    public Batch_group updateObject(Batch_group object);

    public Integer deleteObject(Batch_group object);

    public List<Batch_group> getAllObjects();

    public List<Batch_group> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Batch_group queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Batch_group> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
}
