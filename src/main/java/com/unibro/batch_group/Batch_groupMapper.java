/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch_group;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class Batch_groupMapper implements RowMapper<Batch_group> {

    @Override
    public Batch_group mapRow(ResultSet rs, int rowNum) throws SQLException {
        Batch_group object = new Batch_group();
        object.setId((Integer) rs.getObject("id"));
        object.setTotal_objects((Integer) rs.getObject("total_objects"));
        object.setTotal_cost((Integer) rs.getObject("total_cost"));
        object.setCp((Integer) rs.getObject("cp"));
        object.setCm((Integer) rs.getObject("cm"));
        object.setBv((Integer) rs.getObject("bv"));
        object.setInactive_cp((Integer) rs.getObject("inactive_cp"));
        object.setInactive_cm((Integer) rs.getObject("inactive_cm"));
        object.setInactive_bv((Integer) rs.getObject("inactive_bv"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
