package com.unibro.peacemaker;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.unibro.district.District;
import com.unibro.district.DistrictDAOImpl;
import com.unibro.main.Application;
import com.unibro.province.Province;
import com.unibro.province.ProvinceDAOImpl;
import com.unibro.ward.Ward;
import com.unibro.ward.WardDAOImpl;
import java.util.HashMap;

public class Peacemaker implements Serializable {

    private Integer id = 0;
    private String firstname = "";
    private String lastname = "";
    private String fullname = "";
    private Integer bd_day = 0;
    private Integer bd_month = 0;
    private Integer bd_year = 0;
    private Integer ward_id = 0;
    private Integer district_id = 0;
    private Integer province_id = 0;
    private Integer country_id = 0;
    private Integer househole_ward_id = 0;
    private Integer househole_district_id = 0;
    private Integer househole_province_id = 0;
    private Boolean live_death_state = Boolean.FALSE;
    private Integer income_duration_year = 0;
    private Integer income_duration_month = 0;
    private Integer income = 0;
    private String issue_place = "";
    private Integer batch_id = 0;
    private Integer state = 0;
    private String reason = "";
    private Integer position = 0;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Peacemaker.class);

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("id")
    public Integer getId() {
        return this.id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return this.firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return this.lastname;
    }

    public void setWard_id(Integer ward_id) {
        this.ward_id = ward_id;
    }

    @JsonProperty("ward_id")
    public Integer getWard_id() {
        return this.ward_id;
    }

    public void setDistrict_id(Integer district_id) {
        this.district_id = district_id;
    }

    @JsonProperty("district_id")
    public Integer getDistrict_id() {
        return this.district_id;
    }

    public void setProvince_id(Integer province_id) {
        this.province_id = province_id;
    }

    @JsonProperty("province_id")
    public Integer getProvince_id() {
        return this.province_id;
    }

    public void setCountry_id(Integer country_id) {
        this.country_id = country_id;
    }

    @JsonProperty("country_id")
    public Integer getCountry_id() {
        return this.country_id;
    }

    public void setHousehole_ward_id(Integer househole_ward_id) {
        this.househole_ward_id = househole_ward_id;
    }

    @JsonProperty("househole_ward_id")
    public Integer getHousehole_ward_id() {
        return this.househole_ward_id;
    }

    public void setHousehole_district_id(Integer househole_district_id) {
        this.househole_district_id = househole_district_id;
    }

    @JsonProperty("househole_district_id")
    public Integer getHousehole_district_id() {
        return this.househole_district_id;
    }

    public void setHousehole_province_id(Integer househole_province_id) {
        this.househole_province_id = househole_province_id;
    }

    @JsonProperty("househole_province_id")
    public Integer getHousehole_province_id() {
        return this.househole_province_id;
    }

    public void setLive_death_state(Boolean live_death_state) {
        this.live_death_state = live_death_state;
    }

    @JsonProperty("live_death_state")
    public Boolean getLive_death_state() {
        return this.live_death_state;
    }

    public void setIncome_duration_year(Integer income_duration_year) {
        this.income_duration_year = income_duration_year;
    }

    @JsonProperty("income_duration_year")
    public Integer getIncome_duration_year() {
        return this.income_duration_year;
    }

    public void setIncome_duration_month(Integer income_duration_month) {
        this.income_duration_month = income_duration_month;
    }

    @JsonProperty("income_duration_month")
    public Integer getIncome_duration_month() {
        return this.income_duration_month;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    @JsonProperty("income")
    public Integer getIncome() {
        return this.income;
    }

    public void setIssue_place(String issue_place) {
        this.issue_place = issue_place;
    }

    @JsonProperty("issue_place")
    public String getIssue_place() {
        return this.issue_place;
    }

    public void setBatch_id(Integer batch_id) {
        this.batch_id = batch_id;
    }

    @JsonProperty("batch_id")
    public Integer getBatch_id() {
        return this.batch_id;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("state")
    public Integer getState() {
        return this.state;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    public static Peacemaker getInstant() {
        Peacemaker instant = new Peacemaker();
        instant.setId(0);
        instant.setFirstname("");
        instant.setLastname("");
        instant.setBd_day(0);
        instant.setBd_month(0);
        instant.setBd_year(0);
        instant.setWard_id(0);
        instant.setDistrict_id(0);
        instant.setProvince_id(0);
        instant.setCountry_id(0);
        instant.setHousehole_ward_id(0);
        instant.setHousehole_district_id(0);
        instant.setHousehole_province_id(0);
        instant.setLive_death_state(Boolean.FALSE);
        instant.setIncome_duration_year(0);
        instant.setIncome_duration_month(0);
        instant.setIncome(0);
        instant.setIssue_place("");
        instant.setBatch_id(0);
        instant.setState(0);
        instant.setReason("");
        instant.setPosition(0);
        return instant;
    }

    public Peacemaker() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Peacemaker)) {
            return false;
        }
        Peacemaker compareObj = (Peacemaker) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("id", this.id);
        map.put("firstname", this.firstname);
        map.put("lastname", this.lastname);
        map.put("fullname", this.fullname);
        map.put("bd_day", this.bd_day);
        map.put("bd_month", this.bd_month);
        map.put("bd_year", this.bd_year);
        map.put("ward_id", this.ward_id);
        map.put("district_id", this.district_id);
        map.put("province_id", this.province_id);
        map.put("country_id", this.country_id);
        map.put("househole_ward_id", this.househole_ward_id);
        map.put("househole_district_id", this.househole_district_id);
        map.put("househole_province_id", this.househole_province_id);
        map.put("live_death_state", this.live_death_state);
        map.put("income_duration_year", this.income_duration_year);
        map.put("income_duration_month", this.income_duration_month);
        map.put("income", this.income);
        map.put("issue_place", this.issue_place);
        map.put("batch_id", this.batch_id);
        map.put("state", this.state);
        map.put("reason", this.reason);
        map.put("position", this.position);
        return map;
    }

    public String getUniqueKey() {
        return id.toString();
    }

    /**
     * @return the bd_day
     */
    public Integer getBd_day() {
        return bd_day;
    }

    /**
     * @param bd_day the bd_day to set
     */
    @JsonProperty("bd_day")
    public void setBd_day(Integer bd_day) {
        this.bd_day = bd_day;
    }

    /**
     * @return the bd_month
     */
    @JsonProperty("bd_month")
    public Integer getBd_month() {
        return bd_month;
    }

    /**
     * @param bd_month the bd_month to set
     */
    public void setBd_month(Integer bd_month) {
        this.bd_month = bd_month;
    }

    /**
     * @return the bd_year
     */
    @JsonProperty("bd_year")
    public Integer getBd_year() {
        return bd_year;
    }

    /**
     * @param bd_year the bd_year to set
     */
    public void setBd_year(Integer bd_year) {
        this.bd_year = bd_year;
    }

    /**
     * @return the fullname
     */
    @JsonProperty("fullname")
    public String getFullname() {
        return fullname;
    }

    /**
     * @param fullname the fullname to set
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     * @return the position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    @JsonProperty("position")
    public void setPosition(Integer position) {
        this.position = position;
    }
    
    @JsonIgnore
    public String getBirthday() {
        String ret = this.getBd_year() + "";
        if (this.getBd_month() != 0) {
            ret = this.getBd_month() + "/" + ret;
            if (this.getBd_day() != 0) {
                ret = this.getBd_day() + "/" + ret;
            }
        }
        return ret;
    }   

}
