/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.peacemaker;

import com.unibro.utils.RequestFilter;
import com.unibro.utils.Global;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 *
 * @author THOND
 */
public class PeacemakerDAOImpl implements PeacemakerDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(PeacemakerDAOImpl.class);
    private String exceptionMsg = "";
    private HttpStatus exceptionCode = HttpStatus.OK;
    private String exceptionType = "";

    private static final String columns = "firstname,lastname,fullname,bd_day,bd_month,bd_year,ward_id,district_id,province_id,country_id,househole_ward_id,househole_district_id,househole_province_id,live_death_state,income_duration_year,income_duration_month,income,issue_place,batch_id,state,reason,position";
    private static final String columns_key = "id";

    @Autowired
    private static Environment env;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Peacemaker createObject(final Peacemaker object) {
        try {
            int size = columns.split(",").length;
            final String SQL = "INSERT INTO " + Global.getConfigValue("app.dbname") + ".peacemaker(" + columns + ") VALUES (" + Global.getNumberCharater(size, "?") + ")";;
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, columns.split(","));
                    ps.setObject(1, object.getFirstname());
                    ps.setObject(2, object.getLastname());
                    ps.setObject(3, object.getFullname());
                    ps.setObject(4, object.getBd_day());
                    ps.setObject(5, object.getBd_month());
                    ps.setObject(6, object.getBd_year());
                    ps.setObject(7, object.getWard_id());
                    ps.setObject(8, object.getDistrict_id());
                    ps.setObject(9, object.getProvince_id());
                    ps.setObject(10, object.getCountry_id());
                    ps.setObject(11, object.getHousehole_ward_id());
                    ps.setObject(12, object.getHousehole_district_id());
                    ps.setObject(13, object.getHousehole_province_id());
                    ps.setObject(14, object.getLive_death_state());
                    ps.setObject(15, object.getIncome_duration_year());
                    ps.setObject(16, object.getIncome_duration_month());
                    ps.setObject(17, object.getIncome());
                    ps.setObject(18, object.getIssue_place());
                    ps.setObject(19, object.getBatch_id());
                    ps.setObject(20, object.getState());
                    ps.setObject(21, object.getReason());
                    ps.setObject(22, object.getPosition());
                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            }, keyHolder);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                Long id = (Long) keyHolder.getKey();
                object.setId(id.intValue());
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is created";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Peacemaker getObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE " + Global.getMaskSQL(columns_key, "AND");
            Peacemaker ret = this.jdbcTemplateObject.queryForObject(SQL, values, new PeacemakerMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Peacemaker updateObject(final Peacemaker object) {
        try {
            String[] prepared_arr = (columns + "," + columns_key).split(",");
            final String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET " + Global.getMaskSQL(columns, ",") + " WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) {
                    try {
                        PreparedStatement ps = connection.prepareStatement(SQL, prepared_arr);
                        ps.setObject(1, object.getFirstname());
                        ps.setObject(2, object.getLastname());
                        ps.setObject(3, object.getFullname());
                        ps.setObject(4, object.getBd_day());
                        ps.setObject(5, object.getBd_month());
                        ps.setObject(6, object.getBd_year());
                        ps.setObject(7, object.getWard_id());
                        ps.setObject(8, object.getDistrict_id());
                        ps.setObject(9, object.getProvince_id());
                        ps.setObject(10, object.getCountry_id());
                        ps.setObject(11, object.getHousehole_ward_id());
                        ps.setObject(12, object.getHousehole_district_id());
                        ps.setObject(13, object.getHousehole_province_id());
                        ps.setObject(14, object.getLive_death_state());
                        ps.setObject(15, object.getIncome_duration_year());
                        ps.setObject(16, object.getIncome_duration_month());
                        ps.setObject(17, object.getIncome());
                        ps.setObject(18, object.getIssue_place());
                        ps.setObject(19, object.getBatch_id());
                        ps.setObject(20, object.getState());
                        ps.setObject(21, object.getReason());
                        ps.setObject(22, object.getPosition());
// Primare key
                        ps.setObject(23, object.getId());

                        return ps;
                    } catch (SQLException ex) {
                        logger.error(ex);
                        exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                        exceptionMsg = ex.toString();
                        return null;
                    }
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is updated";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Integer deleteObject(Peacemaker object) {
        try {
            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, object.getId());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer deleteObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".peacemaker where " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, values);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public List<Peacemaker> getAllObjects() {
        try {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".peacemaker";
            List<Peacemaker> objects = jdbcTemplateObject.query(SQL, new PeacemakerMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public List<Peacemaker> loadObjects(String SQL, Object... params) {
        try {
            List<Peacemaker> objects = jdbcTemplateObject.query(SQL, params, new PeacemakerMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Peacemaker queryForObject(String SQL, Object... params) {
        try {
            Peacemaker ret = this.jdbcTemplateObject.queryForObject(SQL, params, new PeacemakerMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Integer getRowCount(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    public Long getTotalData(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Long.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return Long.valueOf(0);
        }
    }

    @Override
    public Integer getTotalRow() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker", Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Long getTotalIncome() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT sum(income) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker", Long.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return Long.valueOf(0);
        }
    }

    @Override
    public Boolean checkObjectExist(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE " + Global.getMaskSQL(columns_key, "AND"), values, Integer.class) > 0;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return false;
        }
    }

    @Override
    public List<Peacemaker> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".peacemaker";
            //No sort
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            return this.loadObjects(SQL);
        } else {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            logger.info("Filter SQL:" + SQL);
            return this.loadObjects(SQL, condition.toArray());
        }

    }

    @Override
    public int filterObjectsSize(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalRow();
        } else {
            String SQL = "SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getRowCount(SQL, condition.toArray());
        }

    }

    /**
     * @return the exceptionMsg
     */
    public String getExceptionMsg() {
        return exceptionMsg;
    }

    /**
     * @param exceptionMsg the exceptionMsg to set
     */
    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    /**
     * @return the exceptionCode
     */
    public HttpStatus getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(HttpStatus exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    /**
     * @return the exceptionType
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * @param exceptionType the exceptionType to set
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    @Override
    public Long filterObjectsIncome(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalData("SELECT sum(income) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker");
        } else {
            String SQL = "SELECT sum(income) FROM " + Global.getConfigValue("app.dbname") + ".peacemaker WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getTotalData(SQL, condition.toArray());
        }

    }

    @Override
    public int updateProvince(int type, PeacemakerPostProvinceUpdateDto dto) {
        try {
            String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET province_id=?, district_id = ?, ward_id = ? WHERE id IN(" + dto.getListId() + ")";
            if (type == 1) {
                SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET househole_province_id=?, househole_district_id = ?, househole_ward_id = ? WHERE id IN(" + dto.getListId() + ")";
            }
            int ret = jdbcTemplateObject.update(SQL, dto.getProvince_id(), dto.getDistrict_id(), dto.getWard_id());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is update";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public int updateAllProvinceId(int type, int current_id, int update_id) {
        if (update_id == 0) {
            return 0;
        }
        if (current_id == update_id) {
            return 0;
        }
        try {
            String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET province_id=? WHERE province_id = ?";
            if (type == 1) {
                SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET househole_province_id=? WHERE househole_province_id = ?";
            }
            int ret = jdbcTemplateObject.update(SQL, update_id, current_id);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is update";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public int updateAllDistrictId(int type, int current_id, int update_id) {
        if (update_id == 0) {
            return 0;
        }
        if (current_id == update_id) {
            return 0;
        }
        try {
            String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET district_id=? WHERE district_id = ?";
            if (type == 1) {
                SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET househole_district_id=? WHERE househole_district_id = ?";
            }
            int ret = jdbcTemplateObject.update(SQL, update_id, current_id);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is update";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public int updateAllWardId(int type, int current_id, int update_id) {
        if (update_id == 0) {
            return 0;
        }
        if (current_id == update_id) {
            return 0;
        }
        try {
            String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET ward_id=? WHERE ward_id = ?";
            if (type == 1) {
                SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET househole_ward_id=? WHERE househole_ward_id = ?";
            }
            int ret = jdbcTemplateObject.update(SQL, update_id, current_id);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is update";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer updateAllProvinceDistrict(int type, int province_id, int district_id, int ward_id, int current_province_id, int current_district_id) {
        try {
            String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET province_id=?, district_id=?, ward_id=? WHERE province_id = ? AND district_id=?";
            if (type == 1) {
                SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".peacemaker SET househole_province_id=?, househole_district_id=?, househole_ward_id=? WHERE househole_province_id = ? AND househole_district_id=?";
            }
            int ret = jdbcTemplateObject.update(SQL, province_id, district_id, ward_id, current_province_id, current_district_id);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is update";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }
}
