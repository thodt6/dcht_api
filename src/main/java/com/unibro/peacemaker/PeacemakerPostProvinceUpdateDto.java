/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.peacemaker;

/**
 *
 * @author LenovoUser
 */
public class PeacemakerPostProvinceUpdateDto {
    private int province_id;
    private int district_id;
    private int ward_id;
    private String listId;

    /**
     * @return the province_id
     */
    public int getProvince_id() {
        return province_id;
    }

    /**
     * @param province_id the province_id to set
     */
    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    /**
     * @return the district_id
     */
    public int getDistrict_id() {
        return district_id;
    }

    /**
     * @param district_id the district_id to set
     */
    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    /**
     * @return the ward_id
     */
    public int getWard_id() {
        return ward_id;
    }

    /**
     * @param ward_id the ward_id to set
     */
    public void setWard_id(int ward_id) {
        this.ward_id = ward_id;
    }

    /**
     * @return the listId
     */
    public String getListId() {
        return listId;
    }

    /**
     * @param listId the listId to set
     */
    public void setListId(String listId) {
        this.listId = listId;
    }
}
