/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.peacemaker;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class PeacemakerMapper implements RowMapper<Peacemaker> {

    @Override
    public Peacemaker mapRow(ResultSet rs, int rowNum) throws SQLException {
        Peacemaker object = new Peacemaker();
        object.setId((Integer) rs.getObject("id"));
        object.setFirstname((String) rs.getObject("firstname"));
        object.setLastname((String) rs.getObject("lastname"));
        object.setFullname((String) rs.getObject("fullname"));
        object.setBd_day((Integer) rs.getObject("bd_day"));
        object.setBd_month((Integer) rs.getObject("bd_month"));
        object.setBd_year((Integer) rs.getObject("bd_year"));
        object.setWard_id((Integer) rs.getObject("ward_id"));
        object.setDistrict_id((Integer) rs.getObject("district_id"));
        object.setProvince_id((Integer) rs.getObject("province_id"));
        object.setCountry_id((Integer) rs.getObject("country_id"));
        object.setHousehole_ward_id((Integer) rs.getObject("househole_ward_id"));
        object.setHousehole_district_id((Integer) rs.getObject("househole_district_id"));
        object.setHousehole_province_id((Integer) rs.getObject("househole_province_id"));
        object.setLive_death_state((Boolean) rs.getObject("live_death_state"));
        object.setIncome_duration_year((Integer) rs.getObject("income_duration_year"));
        object.setIncome_duration_month((Integer) rs.getObject("income_duration_month"));
        object.setIncome((Integer) rs.getObject("income"));
        object.setIssue_place((String) rs.getObject("issue_place"));
        object.setBatch_id((Integer) rs.getObject("batch_id"));
        object.setState((Integer) rs.getObject("state"));
        object.setReason((String) rs.getObject("reason"));
        object.setPosition((Integer) rs.getObject("position"));
        return object;
    }
}
