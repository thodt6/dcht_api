/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.peacemaker;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface PeacemakerDAO {

    public void setDataSource(DataSource ds);

    public Peacemaker createObject(Peacemaker object);

    public Peacemaker getObject(String id);

    public Peacemaker updateObject(Peacemaker object);

    public Integer deleteObject(Peacemaker object);

    public List<Peacemaker> getAllObjects();

    public List<Peacemaker> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Peacemaker queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Long getTotalIncome();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Peacemaker> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);

    public Long filterObjectsIncome(List<RequestFilter> filters);

    public int updateProvince(int type, PeacemakerPostProvinceUpdateDto dto);

    public int updateAllProvinceId(int type, int current_id, int update_id);

    public int updateAllDistrictId(int type, int current_id, int update_id);

    public int updateAllWardId(int type, int current_id, int update_id);    
    
    public Integer updateAllProvinceDistrict(int type,int province_id, int district_id, int ward_id, int current_province_id, int current_district_id);

}
