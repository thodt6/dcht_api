/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.schedule;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unibro.data_exporter.Data_exporter;
import com.unibro.data_exporter.Data_exporterDAOImpl;
import com.unibro.district.District;
import com.unibro.district.DistrictDAOImpl;
import com.unibro.main.Application;
import com.unibro.peacemaker.Peacemaker;
import com.unibro.peacemaker.PeacemakerDAOImpl;
import com.unibro.province.Province;
import com.unibro.province.ProvinceDAOImpl;
import com.unibro.utils.Global;
import com.unibro.utils.RequestFilter;
import com.unibro.ward.Ward;
import com.unibro.ward.WardDAOImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

/**
 *
 * @author LenovoUser
 */
@Service
public class ExportService {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(ExportService.class);

    public void doExport() {
        Data_exporterDAOImpl dao = (Data_exporterDAOImpl) Application.context.getBean("data_exporterDAO");
        List<Data_exporter> list = dao.getDataListExporterByState(0);
        ProvinceDAOImpl dao1 = (ProvinceDAOImpl) Application.context.getBean("provinceDAO");
        DistrictDAOImpl dao2 = (DistrictDAOImpl) Application.context.getBean("districtDAO");
        WardDAOImpl dao3 = (WardDAOImpl) Application.context.getBean("wardDAO");

        List<Province> listProvince = dao1.getAllObjects();
        List<District> listDistrict = dao2.getAllObjects();
        List<Ward> listWard = dao3.getAllObjects();

        if (list != null && !list.isEmpty()) {
            for (Data_exporter exporter : list) {
                this.processExporter(exporter, listProvince, listDistrict, listWard);
                exporter.setState(1);
                dao.updateObject(exporter);
            }
        } else {
//            logger.info("No export found");
        }

    }

    public String getHometown(Integer province_id, Integer district_id, Integer ward_id, List<Province> listProvince, List<District> listDistrict, List<Ward> listWard) {
        String ret = "";
        for (Province p : listProvince) {
            if (p.getProvince_id().intValue() == province_id.intValue()) {
                ret = p.getName();
                break;
            }
        }
        for (District d : listDistrict) {
            if (d.getDistrict_id().intValue() == district_id.intValue()) {
                ret = d.getName() + "/" + ret;
                break;
            }
        }
        for (Ward w : listWard) {
            if (w.getWard_id().intValue() == ward_id.intValue()) {
                ret = w.getName() + "/" + ret;
                break;
            }
        }
        return ret;
    }

    private void processExporter(Data_exporter exporter, List<Province> listProvince, List<District> listDistrict, List<Ward> listWard) {
        try {
            PeacemakerDAOImpl dao = (PeacemakerDAOImpl) Application.context.getBean("peacemakerDAO");
            String filterStr = exporter.getFilterCondition();
            ObjectMapper mapper = new ObjectMapper();
            List<RequestFilter> filters = mapper.readValue(filterStr, new TypeReference<List<RequestFilter>>() {
            });
            int index = 0;
            int size = 1000;
            boolean quit = false;
            File folder = new File(Global.getConfigValue("app.folder") + "/export/" + exporter.getPath());
            folder.mkdirs();
            File excel = new File(Global.getConfigValue("app.folder") + "/export/" + exporter.getPath() + "/" + exporter.getFilename() + ".xlsx");
            String[] columns = Global.getConfigValue("app.columns").split(",");
//            SXSSFWorkbook wb = new SXSSFWorkbook(workbook);
            SXSSFWorkbook workbook = new SXSSFWorkbook();
//            XSSFWorkbook workbook = new XSSFWorkbook(opc);
//            SXSSFWorkbook wb = new SXSSFWorkbook(workbook);
            Sheet sheet = workbook.createSheet(exporter.getFilename());
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 1;
            List<Peacemaker> list = new ArrayList();

            while (!quit) {
                list = dao.filterObjects(index, size, "id", 1, filters);
                if (list == null || list.isEmpty()) {
                    quit = true;
                    logger.info("Finished export");
                } else {
                    logger.info("Loaded " + index + "/" + size);
                    for (Peacemaker p : list) {

                        Row row = sheet.createRow(rowNum++);
                        row.createCell(0).setCellValue(rowNum - 1);
                        row.createCell(1).setCellValue(p.getFullname() == null ? "" : p.getFullname());
                        row.createCell(2).setCellValue(p.getBirthday() == null ? "" : p.getBirthday());
                        String hometown = this.getHometown(p.getProvince_id(), p.getDistrict_id(), p.getWard_id(), listProvince, listDistrict, listWard);
                        String househole = this.getHometown(p.getHousehole_province_id(), p.getHousehole_district_id(), p.getHousehole_ward_id(), listProvince, listDistrict, listWard);
                        row.createCell(3).setCellValue(hometown == null ? "" : hometown);
                        row.createCell(4).setCellValue(househole == null ? "" : househole);
                        if (p.getLive_death_state()) {
                            row.createCell(5).setCellValue("Sống");
                        } else {
                            row.createCell(5).setCellValue("Chết");
                        }
                        row.createCell(6).setCellValue(p.getIncome_duration_year());
                        row.createCell(7).setCellValue(p.getIncome_duration_month());
                        row.createCell(8).setCellValue(p.getIncome());
                        row.createCell(9).setCellValue(p.getIssue_place() == null ? "" : p.getIssue_place());
                    }
                    list.clear();
                }
                index = index + size;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {

                }
            }
            if (sheet instanceof SXSSFSheet) {
                SXSSFSheet sxSheet = (SXSSFSheet) sheet;
                sxSheet.trackAllColumnsForAutoSizing();
                // or track columns individually
            }
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
            FileOutputStream fileOut = new FileOutputStream(excel);
            workbook.write(fileOut);
            fileOut.close();
            // Closing the workbook
            workbook.close();
        } catch (IOException ex) {

        }
    }
}
