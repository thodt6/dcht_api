/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author LenovoUser
 */
@Component
public class Scheduler {

    @Autowired
    private ExportService checkAsyncService;

    @Scheduled(fixedDelay = 1000)
    public void checkTheScedule() {
        checkAsyncService.doExport();
    }

}
