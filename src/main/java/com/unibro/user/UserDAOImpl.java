/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.user;

import com.unibro.utils.RequestFilter;
import com.unibro.utils.Global;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;

/**
 *
 * @author THOND
 */
public class UserDAOImpl implements UserDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(UserDAOImpl.class);
    private String exceptionMsg = "";
    private HttpStatus exceptionCode = HttpStatus.OK;
    private String exceptionType = "";

    private static String columns = "username,default_group,password,state,firstname,lastname,birthday,profile_picture,picture,gender,position,contact_address,contact_number,description,create_user,create_time,update_user,update_time,version,delete_flag";
    private static String columns_key = "userid";

    @Autowired
    private static Environment env;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    @Override
    public User createObject(final User object) {
        try {
            int size = columns.split(",").length;
            final String SQL = "INSERT INTO " + Global.getConfigValue("app.dbname") + ".user(" + columns + ") VALUES (" + Global.getNumberCharater(size, "?") + ")";;
            int ret = jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) {
                    try {
                        PreparedStatement ps = connection.prepareStatement(SQL, columns.split(","));
                        ps.setObject(1, object.getUsername());
                        ps.setObject(2, object.getDefault_group());
                        ps.setObject(3, object.getPassword());
                        ps.setObject(4, object.getState());
                        ps.setObject(5, object.getFirstname());
                        ps.setObject(6, object.getLastname());
                        ps.setObject(7, object.getBirthday());
                        ps.setObject(8, object.getProfile_picture());
                        ps.setObject(9, object.getPicture());
                        ps.setObject(10, object.getGender());
                        ps.setObject(11, object.getPosition());
                        ps.setObject(12, object.getContact_address());
                        ps.setObject(13, object.getContact_number());
                        ps.setObject(14, object.getDescription());
                        ps.setObject(15, object.getCreate_user());
                        ps.setObject(16, object.getCreate_time());
                        ps.setObject(17, object.getUpdate_user());
                        ps.setObject(18, object.getUpdate_time());
                        ps.setObject(19, object.getVersion());
                        ps.setObject(20, object.getDelete_flag());

                        return ps;
                    } catch (SQLException ex) {
                        logger.error(ex);
                        exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                        exceptionMsg = ex.toString();
                        return null;
                    }
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is created";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public User getObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".user WHERE " + Global.getMaskSQL(columns_key, "AND");
            User ret = this.jdbcTemplateObject.queryForObject(SQL, values, new UserMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public User updateObject(final User object) {
        try {
            String[] prepared_arr = (columns + "," + columns_key).split(",");
            final String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".user SET " + Global.getMaskSQL(columns, ",") + " WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) {
                    try {
                        PreparedStatement ps = connection.prepareStatement(SQL, prepared_arr);
                        ps.setObject(1, object.getUsername());
                        ps.setObject(2, object.getDefault_group());
                        ps.setObject(3, object.getPassword());
                        ps.setObject(4, object.getState());
                        ps.setObject(5, object.getFirstname());
                        ps.setObject(6, object.getLastname());
                        ps.setObject(7, object.getBirthday());
                        ps.setObject(8, object.getProfile_picture());
                        ps.setObject(9, object.getPicture());
                        ps.setObject(10, object.getGender());
                        ps.setObject(11, object.getPosition());
                        ps.setObject(12, object.getContact_address());
                        ps.setObject(13, object.getContact_number());
                        ps.setObject(14, object.getDescription());
                        ps.setObject(15, object.getCreate_user());
                        ps.setObject(16, object.getCreate_time());
                        ps.setObject(17, object.getUpdate_user());
                        ps.setObject(18, object.getUpdate_time());
                        ps.setObject(19, object.getVersion());
                        ps.setObject(20, object.getDelete_flag());
// Primare key
                        ps.setObject(21, object.getUserid());

                        return ps;
                    } catch (SQLException ex) {
                        logger.error(ex);
                        exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                        exceptionMsg = ex.toString();
                        return null;
                    }
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is updated";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Integer deleteObject(User object) {
        try {
            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".user WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, object.getUserid());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer deleteObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".user where " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, values);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public List<User> getAllObjects() {
        try {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".user";
            List<User> objects = jdbcTemplateObject.query(SQL, new UserMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public List<User> loadObjects(String SQL, Object... params) {
        try {
            List<User> objects = jdbcTemplateObject.query(SQL, params, new UserMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public User queryForObject(String SQL, Object... params) {
        try {
            User ret = this.jdbcTemplateObject.queryForObject(SQL, params, new UserMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Integer getRowCount(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer getTotalRow() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".user", Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Boolean checkObjectExist(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".user WHERE " + Global.getMaskSQL(columns_key, "AND"), values, Integer.class) > 0;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return false;
        }
    }

    @Override
    public List<User> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".user";
            //No sort
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            return this.loadObjects(SQL);
        } else {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".user WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            logger.info("Filter SQL:" + SQL);
            return this.loadObjects(SQL, condition.toArray());
        }

    }

    @Override
    public int filterObjectsSize(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalRow();
        } else {
            String SQL = "SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".user WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getRowCount(SQL, condition.toArray());
        }

    }

    /**
     * @return the exceptionMsg
     */
    public String getExceptionMsg() {
        return exceptionMsg;
    }

    /**
     * @param exceptionMsg the exceptionMsg to set
     */
    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    /**
     * @return the exceptionCode
     */
    public HttpStatus getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(HttpStatus exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    /**
     * @return the exceptionType
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * @param exceptionType the exceptionType to set
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    @Override
    public User getUserByUsername(String username) {
        String SQL = "SELECT * FROM dcht.user WHERE username=?";
        return this.queryForObject(SQL, username);
    }

}
