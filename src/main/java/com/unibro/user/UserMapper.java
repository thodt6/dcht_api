/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.user;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User object = new User();
        object.setUserid((Integer) rs.getObject("userid"));
        object.setUsername((String) rs.getObject("username"));
        object.setDefault_group((Integer) rs.getObject("default_group"));
        object.setPassword((String) rs.getObject("password"));
        object.setState((Integer) rs.getObject("state"));
        object.setFirstname((String) rs.getObject("firstname"));
        object.setLastname((String) rs.getObject("lastname"));
        object.setBirthday((java.util.Date) rs.getObject("birthday"));
        object.setProfile_picture((String) rs.getObject("profile_picture"));
        object.setPicture((String) rs.getObject("picture"));
        object.setGender((String) rs.getObject("gender"));
        object.setPosition((String) rs.getObject("position"));
        object.setContact_address((String) rs.getObject("contact_address"));
        object.setContact_number((String) rs.getObject("contact_number"));
        object.setDescription((String) rs.getObject("description"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
