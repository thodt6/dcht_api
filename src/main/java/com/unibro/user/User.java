package com.unibro.user;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class User implements Serializable {

    private Integer userid = 0;
    private String username = "";
    private Integer default_group = 0;
    private String password = "";
    private Integer state = 0;
    private String firstname = "";
    private String lastname = "";
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date birthday = new java.util.Date();
    private String profile_picture = "";
    private String picture = "";
    private String gender = "";
    private String position = "";
    private String contact_address = "";
    private String contact_number = "";
    private String description = "";
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_time = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(User.class);

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @JsonProperty("userid")
    public Integer getUserid() {
        return this.userid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("username")
    public String getUsername() {
        return this.username;
    }

    public void setDefault_group(Integer default_group) {
        this.default_group = default_group;
    }

    @JsonProperty("default_group")
    public Integer getDefault_group() {
        return this.default_group;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("password")
    public String getPassword() {
        return this.password;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("state")
    public Integer getState() {
        return this.state;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return this.firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return this.lastname;
    }

    public void setBirthday(java.util.Date birthday) {
        this.birthday = birthday;
    }

    @JsonProperty("birthday")
    public java.util.Date getBirthday() {
        return this.birthday;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    @JsonProperty("profile_picture")
    public String getProfile_picture() {
        return this.profile_picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @JsonProperty("picture")
    public String getPicture() {
        return this.picture;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("gender")
    public String getGender() {
        return this.gender;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @JsonProperty("position")
    public String getPosition() {
        return this.position;
    }

    public void setContact_address(String contact_address) {
        this.contact_address = contact_address;
    }

    @JsonProperty("contact_address")
    public String getContact_address() {
        return this.contact_address;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    @JsonProperty("contact_number")
    public String getContact_number() {
        return this.contact_number;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_time(java.util.Date create_time) {
        this.create_time = create_time;
    }

    @JsonProperty("create_time")
    public java.util.Date getCreate_time() {
        return this.create_time;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public static User getInstant() {
        User instant = new User();
        instant.setUserid(0);
        instant.setUsername("");
        instant.setDefault_group(0);
        instant.setPassword("");
        instant.setState(0);
        instant.setFirstname("");
        instant.setLastname("");
        instant.setBirthday(new java.util.Date());
        instant.setProfile_picture("");
        instant.setPicture("");
        instant.setGender("");
        instant.setPosition("");
        instant.setContact_address("");
        instant.setContact_number("");
        instant.setDescription("");
        instant.setCreate_user(0);
        instant.setCreate_time(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);

        return instant;
    }

    public User() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User compareObj = (User) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("userid", this.userid);
        map.put("username", this.username);
        map.put("default_group", this.default_group);
        map.put("password", this.password);
        map.put("state", this.state);
        map.put("firstname", this.firstname);
        map.put("lastname", this.lastname);
        map.put("birthday", this.birthday);
        map.put("profile_picture", this.profile_picture);
        map.put("picture", this.picture);
        map.put("gender", this.gender);
        map.put("position", this.position);
        map.put("contact_address", this.contact_address);
        map.put("contact_number", this.contact_number);
        map.put("description", this.description);
        map.put("create_user", this.create_user);
        map.put("create_time", this.create_time);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);

        return map;
    }

    public String getUniqueKey() {
        return userid.toString();
    }

}
