/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.function;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface FunctionDAO {

    public void setDataSource(DataSource ds);

    public Function createObject(Function object);

    public Function getObject(String id);

    public Function updateObject(Function object);

    public Integer deleteObject(Function object);

    public List<Function> getAllObjects();

    public List<Function> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Function queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Function> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
    
    public Function getFunctionByUri(String uri);
}
