/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.function;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class FunctionMapper implements RowMapper<Function> {

    @Override
    public Function mapRow(ResultSet rs, int rowNum) throws SQLException {
        Function object = new Function();
        object.setId((Integer) rs.getObject("id"));
        object.setName((String) rs.getObject("name"));
        object.setUri((String) rs.getObject("uri"));
        object.setState((Integer) rs.getObject("state"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
