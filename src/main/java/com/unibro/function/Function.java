package com.unibro.function;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Function implements Serializable {

    private Integer id = 0;
    private String name = "";
    private String uri = "";
    private Integer state = 0;
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_time = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Function.class);

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("id")
    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @JsonProperty("uri")
    public String getUri() {
        return this.uri;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("state")
    public Integer getState() {
        return this.state;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_time(java.util.Date create_time) {
        this.create_time = create_time;
    }

    @JsonProperty("create_time")
    public java.util.Date getCreate_time() {
        return this.create_time;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public static Function getInstant() {
        Function instant = new Function();
        instant.setId(0);
        instant.setName("");
        instant.setUri("");
        instant.setState(0);
        instant.setCreate_user(0);
        instant.setCreate_time(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);

        return instant;
    }

    public Function() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Function)) {
            return false;
        }
        Function compareObj = (Function) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("id", this.id);
        map.put("name", this.name);
        map.put("uri", this.uri);
        map.put("state", this.state);
        map.put("create_user", this.create_user);
        map.put("create_time", this.create_time);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);

        return map;
    }

    public String getUniqueKey() {
        return id.toString();
    }

}
