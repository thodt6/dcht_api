/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.data_exporter;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class Data_exporterMapper implements RowMapper<Data_exporter> {

    @Override
    public Data_exporter mapRow(ResultSet rs, int rowNum) throws SQLException {
        Data_exporter object = new Data_exporter();
        object.setId((Integer) rs.getObject("id"));
        object.setFilePath((String) rs.getObject("filePath"));
        object.setDescription((String) rs.getObject("description"));
        object.setFilterCondition((String) rs.getObject("filterCondition"));
        object.setState((Integer) rs.getObject("state"));
        object.setCreated_id((Integer) rs.getObject("created_id"));
        object.setCreated_time((java.util.Date) rs.getObject("created_time"));

        return object;
    }
}
