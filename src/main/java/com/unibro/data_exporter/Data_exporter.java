package com.unibro.data_exporter;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Data_exporter implements Serializable {

    private Integer id = 0;
    private String filePath = "";
    private String description = "";
    private String filterCondition = "";
    private Integer state = 0;
    private Integer created_id = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date created_time = new java.util.Date();

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Data_exporter.class);

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("id")
    public Integer getId() {
        return this.id;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @JsonProperty("filePath")
    public String getFilePath() {
        return this.filePath;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setFilterCondition(String filterCondition) {
        this.filterCondition = filterCondition;
    }

    @JsonProperty("filterCondition")
    public String getFilterCondition() {
        return this.filterCondition;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("state")
    public Integer getState() {
        return this.state;
    }

    public void setCreated_id(Integer created_id) {
        this.created_id = created_id;
    }

    @JsonProperty("created_id")
    public Integer getCreated_id() {
        return this.created_id;
    }

    public void setCreated_time(java.util.Date created_time) {
        this.created_time = created_time;
    }

    @JsonProperty("created_time")
    public java.util.Date getCreated_time() {
        return this.created_time;
    }

    public static Data_exporter getInstant() {
        Data_exporter instant = new Data_exporter();
        instant.setId(0);
        instant.setFilePath("");
        instant.setDescription("");
        instant.setFilterCondition("");
        instant.setState(0);
        instant.setCreated_id(0);
        instant.setCreated_time(new java.util.Date());

        return instant;
    }

    public Data_exporter() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Data_exporter)) {
            return false;
        }
        Data_exporter compareObj = (Data_exporter) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("id", this.id);
        map.put("filePath", this.filePath);
        map.put("description", this.description);
        map.put("filterCondition", this.filterCondition);
        map.put("state", this.state);
        map.put("created_id", this.created_id);
        map.put("created_time", this.created_time);

        return map;
    }

    public String getUniqueKey() {
        return id.toString();
    }

    @JsonIgnore
    public String getPath() {
        int index = this.getFilePath().lastIndexOf("/");
        if (index > 0) {
            return this.getFilePath().substring(0, index);
        } else {
            return this.getFilePath();
        }
    }
    @JsonIgnore
    public String getFilename() {
        int index = this.getFilePath().lastIndexOf("/");
        if (index > 0) {
            return this.getFilePath().substring(index+1);
        } else {
            return this.getFilePath();
        }
    }

}
