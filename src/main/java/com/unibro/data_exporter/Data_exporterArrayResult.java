/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.data_exporter;

import java.util.List;

/**
 *
 * @author THOND
 */
public class Data_exporterArrayResult {

    private List<Data_exporter> data;
    private Integer size;

    public Data_exporterArrayResult(List<Data_exporter> data, Integer size) {
        this.data = data;
        this.size = size;
    }

    /**
     * @return the data
     */
    public List<Data_exporter> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Data_exporter> data) {
        this.data = data;
    }

    /**
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
