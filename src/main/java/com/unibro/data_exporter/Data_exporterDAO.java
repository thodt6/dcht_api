/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.data_exporter;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface Data_exporterDAO {

    public void setDataSource(DataSource ds);

    public Data_exporter createObject(Data_exporter object);

    public Data_exporter getObject(String id);

    public Data_exporter updateObject(Data_exporter object);

    public Integer deleteObject(Data_exporter object);

    public List<Data_exporter> getAllObjects();

    public List<Data_exporter> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Data_exporter queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Data_exporter> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
    
    public List<Data_exporter> getDataListExporterByState(int state);
}
