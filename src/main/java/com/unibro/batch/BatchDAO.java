/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface BatchDAO {

    public void setDataSource(DataSource ds);

    public Batch createObject(Batch object);

    public Batch getObject(String id);

    public Batch updateObject(Batch object);

    public Integer deleteObject(Batch object);

    public List<Batch> getAllObjects();

    public List<Batch> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Batch queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Batch> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
}
