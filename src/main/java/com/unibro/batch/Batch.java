package com.unibro.batch;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;

public class Batch implements Serializable {

    private Integer batch_id = 0;
    private Integer batch_no = 0;
    private String name = "";
    private String create_batch = "";
    private String create_position = "";
    private String approve_batch = "";
    private String approve_position = "";
    private Integer state = 0;
    private String origin_file = "";
    private String formated_file = "";
    private Integer total_objects = 0;
    private Integer total_cost = 0;
    private Integer cp = 0;
    private Integer cm = 0;
    private Integer bv = 0;
    private Integer inactive_cp = 0;
    private Integer inactive_cm = 0;
    private Integer inactive_bv = 0;
    private Integer decision_id = 0;
    private Integer create_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date create_date = new java.util.Date();
    private Integer update_user = 0;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private java.util.Date update_time = new java.util.Date();
    private Integer version = 0;
    private Boolean delete_flag = Boolean.FALSE;
    private Integer batch_group_id = 0;
    private Integer position = 0;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Batch.class);

    public void setBatch_id(Integer batch_id) {
        this.batch_id = batch_id;
    }

    @JsonProperty("batch_id")
    public Integer getBatch_id() {
        return this.batch_id;
    }

    public void setBatch_no(Integer batch_no) {
        this.batch_no = batch_no;
    }

    @JsonProperty("batch_no")
    public Integer getBatch_no() {
        return this.batch_no;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setCreate_batch(String create_batch) {
        this.create_batch = create_batch;
    }

    @JsonProperty("create_batch")
    public String getCreate_batch() {
        return this.create_batch;
    }

    public void setCreate_position(String create_position) {
        this.create_position = create_position;
    }

    @JsonProperty("create_position")
    public String getCreate_position() {
        return this.create_position;
    }

    public void setApprove_batch(String approve_batch) {
        this.approve_batch = approve_batch;
    }

    @JsonProperty("approve_batch")
    public String getApprove_batch() {
        return this.approve_batch;
    }

    public void setApprove_position(String approve_position) {
        this.approve_position = approve_position;
    }

    @JsonProperty("approve_position")
    public String getApprove_position() {
        return this.approve_position;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @JsonProperty("state")
    public Integer getState() {
        return this.state;
    }

    public void setOrigin_file(String origin_file) {
        this.origin_file = origin_file;
    }

    @JsonProperty("origin_file")
    public String getOrigin_file() {
        return this.origin_file;
    }

    public void setFormated_file(String formated_file) {
        this.formated_file = formated_file;
    }

    @JsonProperty("formated_file")
    public String getFormated_file() {
        return this.formated_file;
    }

    public void setTotal_objects(Integer total_objects) {
        this.total_objects = total_objects;
    }

    @JsonProperty("total_objects")
    public Integer getTotal_objects() {
        return this.total_objects;
    }

    public void setTotal_cost(Integer total_cost) {
        this.total_cost = total_cost;
    }

    @JsonProperty("total_cost")
    public Integer getTotal_cost() {
        return this.total_cost;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    @JsonProperty("cp")
    public Integer getCp() {
        return this.cp;
    }

    public void setCm(Integer cm) {
        this.cm = cm;
    }

    @JsonProperty("cm")
    public Integer getCm() {
        return this.cm;
    }

    public void setBv(Integer bv) {
        this.bv = bv;
    }

    @JsonProperty("bv")
    public Integer getBv() {
        return this.bv;
    }

    public void setInactive_cp(Integer inactive_cp) {
        this.inactive_cp = inactive_cp;
    }

    @JsonProperty("inactive_cp")
    public Integer getInactive_cp() {
        return this.inactive_cp;
    }

    public void setInactive_cm(Integer inactive_cm) {
        this.inactive_cm = inactive_cm;
    }

    @JsonProperty("inactive_cm")
    public Integer getInactive_cm() {
        return this.inactive_cm;
    }

    public void setInactive_bv(Integer inactive_bv) {
        this.inactive_bv = inactive_bv;
    }

    @JsonProperty("inactive_bv")
    public Integer getInactive_bv() {
        return this.inactive_bv;
    }

    public void setDecision_id(Integer decision_id) {
        this.decision_id = decision_id;
    }

    @JsonProperty("decision_id")
    public Integer getDecision_id() {
        return this.decision_id;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    @JsonProperty("create_user")
    public Integer getCreate_user() {
        return this.create_user;
    }

    public void setCreate_date(java.util.Date create_date) {
        this.create_date = create_date;
    }

    @JsonProperty("create_date")
    public java.util.Date getCreate_date() {
        return this.create_date;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    @JsonProperty("update_user")
    public Integer getUpdate_user() {
        return this.update_user;
    }

    public void setUpdate_time(java.util.Date update_time) {
        this.update_time = update_time;
    }

    @JsonProperty("update_time")
    public java.util.Date getUpdate_time() {
        return this.update_time;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return this.version;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    @JsonProperty("delete_flag")
    public Boolean getDelete_flag() {
        return this.delete_flag;
    }

    public void setBatch_group_id(Integer batch_group_id) {
        this.batch_group_id = batch_group_id;
    }

    @JsonProperty("batch_group_id")
    public Integer getBatch_group_id() {
        return this.batch_group_id;
    }

    public static Batch getInstant() {
        Batch instant = new Batch();
        instant.setBatch_id(0);
        instant.setBatch_no(0);
        instant.setName("");
        instant.setCreate_batch("");
        instant.setCreate_position("");
        instant.setApprove_batch("");
        instant.setApprove_position("");
        instant.setState(0);
        instant.setOrigin_file("");
        instant.setFormated_file("");
        instant.setTotal_objects(0);
        instant.setTotal_cost(0);
        instant.setCp(0);
        instant.setCm(0);
        instant.setBv(0);
        instant.setInactive_cp(0);
        instant.setInactive_cm(0);
        instant.setInactive_bv(0);
        instant.setDecision_id(0);
        instant.setCreate_user(0);
        instant.setCreate_date(new java.util.Date());
        instant.setUpdate_user(0);
        instant.setUpdate_time(new java.util.Date());
        instant.setVersion(0);
        instant.setDelete_flag(Boolean.FALSE);
        instant.setBatch_group_id(0);

        return instant;
    }

    public Batch() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Batch)) {
            return false;
        }
        Batch compareObj = (Batch) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("batch_id", this.batch_id);
        map.put("batch_no", this.batch_no);
        map.put("name", this.name);
        map.put("create_batch", this.create_batch);
        map.put("create_position", this.create_position);
        map.put("approve_batch", this.approve_batch);
        map.put("approve_position", this.approve_position);
        map.put("state", this.state);
        map.put("origin_file", this.origin_file);
        map.put("formated_file", this.formated_file);
        map.put("total_objects", this.total_objects);
        map.put("total_cost", this.total_cost);
        map.put("cp", this.cp);
        map.put("cm", this.cm);
        map.put("bv", this.bv);
        map.put("inactive_cp", this.inactive_cp);
        map.put("inactive_cm", this.inactive_cm);
        map.put("inactive_bv", this.inactive_bv);
        map.put("decision_id", this.decision_id);
        map.put("create_user", this.create_user);
        map.put("create_date", this.create_date);
        map.put("update_user", this.update_user);
        map.put("update_time", this.update_time);
        map.put("version", this.version);
        map.put("delete_flag", this.delete_flag);
        map.put("batch_group_id", this.batch_group_id);

        return map;
    }

    public String getUniqueKey() {
        return batch_id.toString();
    }

    /**
     * @return the position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

}
