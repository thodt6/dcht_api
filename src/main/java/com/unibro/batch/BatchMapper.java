/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class BatchMapper implements RowMapper<Batch> {

    @Override
    public Batch mapRow(ResultSet rs, int rowNum) throws SQLException {
        Batch object = new Batch();
        object.setBatch_id((Integer) rs.getObject("batch_id"));
        object.setBatch_no((Integer) rs.getObject("batch_no"));
        object.setName((String) rs.getObject("name"));
        object.setCreate_batch((String) rs.getObject("create_batch"));
        object.setCreate_position((String) rs.getObject("create_position"));
        object.setApprove_batch((String) rs.getObject("approve_batch"));
        object.setApprove_position((String) rs.getObject("approve_position"));
        object.setState((Integer) rs.getObject("state"));
        object.setOrigin_file((String) rs.getObject("origin_file"));
        object.setFormated_file((String) rs.getObject("formated_file"));
        object.setTotal_objects((Integer) rs.getObject("total_objects"));
        object.setTotal_cost((Integer) rs.getObject("total_cost"));
        object.setCp((Integer) rs.getObject("cp"));
        object.setCm((Integer) rs.getObject("cm"));
        object.setBv((Integer) rs.getObject("bv"));
        object.setInactive_cp((Integer) rs.getObject("inactive_cp"));
        object.setInactive_cm((Integer) rs.getObject("inactive_cm"));
        object.setInactive_bv((Integer) rs.getObject("inactive_bv"));
        object.setDecision_id((Integer) rs.getObject("decision_id"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_date((java.util.Date) rs.getObject("create_date"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));
        object.setBatch_group_id((Integer) rs.getObject("batch_group_id"));
        object.setPosition((Integer) rs.getObject("position"));
        return object;
    }
}
