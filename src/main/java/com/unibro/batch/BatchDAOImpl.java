/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.batch;

import com.unibro.utils.RequestFilter;
import com.unibro.utils.Global;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 *
 * @author THOND
 */
public class BatchDAOImpl implements BatchDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(BatchDAOImpl.class);
    private String exceptionMsg = "";
    private HttpStatus exceptionCode = HttpStatus.OK;
    private String exceptionType = "";

    private static final String columns = "batch_no,name,create_batch,create_position,approve_batch,approve_position,state,origin_file,formated_file,total_objects,total_cost,cp,cm,bv,inactive_cp,inactive_cm,inactive_bv,decision_id,create_user,create_date,update_user,update_time,version,delete_flag,batch_group_id,position";
    private static final String columns_key = "batch_id";

    @Autowired
    private static Environment env;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Batch createObject(final Batch object) {
        try {
            int size = columns.split(",").length;
            final String SQL = "INSERT INTO " + Global.getConfigValue("app.dbname") + ".batch(" + columns + ") VALUES (" + Global.getNumberCharater(size, "?") + ")";;
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, columns.split(","));
                    ps.setObject(1, object.getBatch_no());
                    ps.setObject(2, object.getName());
                    ps.setObject(3, object.getCreate_batch());
                    ps.setObject(4, object.getCreate_position());
                    ps.setObject(5, object.getApprove_batch());
                    ps.setObject(6, object.getApprove_position());
                    ps.setObject(7, object.getState());
                    ps.setObject(8, object.getOrigin_file());
                    ps.setObject(9, object.getFormated_file());
                    ps.setObject(10, object.getTotal_objects());
                    ps.setObject(11, object.getTotal_cost());
                    ps.setObject(12, object.getCp());
                    ps.setObject(13, object.getCm());
                    ps.setObject(14, object.getBv());
                    ps.setObject(15, object.getInactive_cp());
                    ps.setObject(16, object.getInactive_cm());
                    ps.setObject(17, object.getInactive_bv());
                    ps.setObject(18, object.getDecision_id());
                    ps.setObject(19, object.getCreate_user());
                    ps.setObject(20, object.getCreate_date());
                    ps.setObject(21, object.getUpdate_user());
                    ps.setObject(22, object.getUpdate_time());
                    ps.setObject(23, object.getVersion());
                    ps.setObject(24, object.getDelete_flag());
                    ps.setObject(25, object.getBatch_group_id());
                    ps.setObject(26, object.getPosition());
                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            }, keyHolder);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                Long id = (Long) keyHolder.getKey();
                object.setBatch_id(id.intValue());
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is created";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Batch getObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".batch WHERE " + Global.getMaskSQL(columns_key, "AND");
            Batch ret = this.jdbcTemplateObject.queryForObject(SQL, values, new BatchMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Batch updateObject(final Batch object) {
        try {
            String[] prepared_arr = (columns + "," + columns_key).split(",");
            final String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".batch SET " + Global.getMaskSQL(columns, ",") + " WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, prepared_arr);
                    ps.setObject(1, object.getBatch_no());
                    ps.setObject(2, object.getName());
                    ps.setObject(3, object.getCreate_batch());
                    ps.setObject(4, object.getCreate_position());
                    ps.setObject(5, object.getApprove_batch());
                    ps.setObject(6, object.getApprove_position());
                    ps.setObject(7, object.getState());
                    ps.setObject(8, object.getOrigin_file());
                    ps.setObject(9, object.getFormated_file());
                    ps.setObject(10, object.getTotal_objects());
                    ps.setObject(11, object.getTotal_cost());
                    ps.setObject(12, object.getCp());
                    ps.setObject(13, object.getCm());
                    ps.setObject(14, object.getBv());
                    ps.setObject(15, object.getInactive_cp());
                    ps.setObject(16, object.getInactive_cm());
                    ps.setObject(17, object.getInactive_bv());
                    ps.setObject(18, object.getDecision_id());
                    ps.setObject(19, object.getCreate_user());
                    ps.setObject(20, object.getCreate_date());
                    ps.setObject(21, object.getUpdate_user());
                    ps.setObject(22, object.getUpdate_time());
                    ps.setObject(23, object.getVersion());
                    ps.setObject(24, object.getDelete_flag());
                    ps.setObject(25, object.getBatch_group_id());
                    ps.setObject(26, object.getPosition());
// Primare key
                    ps.setObject(27, object.getBatch_id());

                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is updated";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Integer deleteObject(Batch object) {
        try {
            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".batch WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, object.getBatch_id());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer deleteObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".batch where " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, values);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public List<Batch> getAllObjects() {
        try {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".batch";
            List<Batch> objects = jdbcTemplateObject.query(SQL, new BatchMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public List<Batch> loadObjects(String SQL, Object... params) {
        try {
            List<Batch> objects = jdbcTemplateObject.query(SQL, params, new BatchMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Batch queryForObject(String SQL, Object... params) {
        try {
            Batch ret = this.jdbcTemplateObject.queryForObject(SQL, params, new BatchMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Integer getRowCount(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer getTotalRow() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".batch", Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Boolean checkObjectExist(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".batch WHERE " + Global.getMaskSQL(columns_key, "AND"), values, Integer.class) > 0;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return false;
        }
    }

    @Override
    public List<Batch> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".batch";
            //No sort
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            return this.loadObjects(SQL);
        } else {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".batch WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            logger.info("Filter SQL:" + SQL);
            return this.loadObjects(SQL, condition.toArray());
        }

    }

    @Override
    public int filterObjectsSize(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalRow();
        } else {
            String SQL = "SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".batch WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getRowCount(SQL, condition.toArray());
        }

    }

    /**
     * @return the exceptionMsg
     */
    public String getExceptionMsg() {
        return exceptionMsg;
    }

    /**
     * @param exceptionMsg the exceptionMsg to set
     */
    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    /**
     * @return the exceptionCode
     */
    public HttpStatus getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(HttpStatus exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    /**
     * @return the exceptionType
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * @param exceptionType the exceptionType to set
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

}
