/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.country;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class CountryMapper implements RowMapper<Country> {

    @Override
    public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
        Country object = new Country();
        object.setCode((String) rs.getObject("code"));
        object.setName((String) rs.getObject("name"));
        object.setCalling_code((String) rs.getObject("calling_code"));
        object.setTop_domain((String) rs.getObject("top_domain"));
        object.setCapital((String) rs.getObject("capital"));

        return object;
    }
}
