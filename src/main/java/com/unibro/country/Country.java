package com.unibro.country;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Country implements Serializable {

    private String code = Global.getRandomString();
    private String name = "";
    private String calling_code = "";
    private String top_domain = "";
    private String capital = "";

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Country.class);

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("code")
    public String getCode() {
        return this.code;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setCalling_code(String calling_code) {
        this.calling_code = calling_code;
    }

    @JsonProperty("calling_code")
    public String getCalling_code() {
        return this.calling_code;
    }

    public void setTop_domain(String top_domain) {
        this.top_domain = top_domain;
    }

    @JsonProperty("top_domain")
    public String getTop_domain() {
        return this.top_domain;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @JsonProperty("capital")
    public String getCapital() {
        return this.capital;
    }

    public static Country getInstant() {
        Country instant = new Country();
        instant.setCode(Global.getRandomString());
        instant.setName("");
        instant.setCalling_code("");
        instant.setTop_domain("");
        instant.setCapital("");

        return instant;
    }

    public Country() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Country)) {
            return false;
        }
        Country compareObj = (Country) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("code", this.code);
        map.put("name", this.name);
        map.put("calling_code", this.calling_code);
        map.put("top_domain", this.top_domain);
        map.put("capital", this.capital);

        return map;
    }

    public String getUniqueKey() {
        return code;
    }

}
