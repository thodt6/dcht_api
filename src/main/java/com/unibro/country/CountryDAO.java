/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.country;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface CountryDAO {

    public void setDataSource(DataSource ds);

    public Country createObject(Country object);

    public Country getObject(String id);

    public Country updateObject(Country object);

    public Integer deleteObject(Country object);

    public List<Country> getAllObjects();

    public List<Country> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Country queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Country> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
}
