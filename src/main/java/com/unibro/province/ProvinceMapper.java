/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.province;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class ProvinceMapper implements RowMapper<Province> {

    @Override
    public Province mapRow(ResultSet rs, int rowNum) throws SQLException {
        Province object = new Province();
        object.setProvince_id((Integer) rs.getObject("province_id"));
        object.setProvince_code((String) rs.getObject("province_code"));
        object.setName((String) rs.getObject("name"));
        object.setPopulation((Integer) rs.getObject("population"));
        object.setCountry_code((String) rs.getObject("country_code"));

        return object;
    }
}
