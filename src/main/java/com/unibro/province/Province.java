package com.unibro.province;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Province implements Serializable {

    private Integer province_id = 0;
    private String province_code = "";
    private String name = "";
    private Integer population = 0;
    private String country_code = "";

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Province.class);

    public void setProvince_id(Integer province_id) {
        this.province_id = province_id;
    }

    @JsonProperty("province_id")
    public Integer getProvince_id() {
        return this.province_id;
    }

    public void setProvince_code(String province_code) {
        this.province_code = province_code;
    }

    @JsonProperty("province_code")
    public String getProvince_code() {
        return this.province_code;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    @JsonProperty("population")
    public Integer getPopulation() {
        return this.population;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    @JsonProperty("country_code")
    public String getCountry_code() {
        return this.country_code;
    }

    public static Province getInstant() {
        Province instant = new Province();
        instant.setProvince_id(0);
        instant.setProvince_code("");
        instant.setName("");
        instant.setPopulation(0);
        instant.setCountry_code("");

        return instant;
    }

    public Province() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Province)) {
            return false;
        }
        Province compareObj = (Province) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("province_id", this.province_id);
        map.put("province_code", this.province_code);
        map.put("name", this.name);
        map.put("population", this.population);
        map.put("country_code", this.country_code);

        return map;
    }

    public String getUniqueKey() {
        return province_id.toString();
    }

}
