/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.province;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface ProvinceDAO {

    public void setDataSource(DataSource ds);

    public Province createObject(Province object);

    public Province getObject(String id);

    public Province updateObject(Province object);

    public Integer deleteObject(Province object);

    public List<Province> getAllObjects();

    public List<Province> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Province queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Province> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);

    public Province getByName(String name);

    public Integer updateProvince(int current_id, int update_id);

}
