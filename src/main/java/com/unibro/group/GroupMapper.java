/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.group;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class GroupMapper implements RowMapper<Group> {

    @Override
    public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
        Group object = new Group();
        object.setGroupid((Integer) rs.getObject("groupid"));
        object.setGroup_name((String) rs.getObject("group_name"));
        object.setDescription((String) rs.getObject("description"));
        object.setDefault_uri((String) rs.getObject("default_uri"));
        object.setCreate_user((Integer) rs.getObject("create_user"));
        object.setCreate_time((java.util.Date) rs.getObject("create_time"));
        object.setUpdate_user((Integer) rs.getObject("update_user"));
        object.setUpdate_time((java.util.Date) rs.getObject("update_time"));
        object.setVersion((Integer) rs.getObject("version"));
        object.setDelete_flag((Boolean) rs.getObject("delete_flag"));

        return object;
    }
}
