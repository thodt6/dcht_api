/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.ward;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author THOND
 */
public class WardMapper implements RowMapper<Ward> {

    @Override
    public Ward mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ward object = new Ward();
        object.setWard_id((Integer) rs.getObject("ward_id"));
        object.setName((String) rs.getObject("name"));
        object.setPopulation((Integer) rs.getObject("population"));
        object.setDistrict_id((Integer) rs.getObject("district_id"));

        return object;
    }
}
