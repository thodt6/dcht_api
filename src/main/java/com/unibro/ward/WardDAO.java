/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.ward;

import com.unibro.utils.RequestFilter;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author THOND
 */
public interface WardDAO {

    public void setDataSource(DataSource ds);

    public Ward createObject(Ward object);

    public Ward getObject(String id);

    public Ward updateObject(Ward object);

    public Integer deleteObject(Ward object);

    public List<Ward> getAllObjects();

    public List<Ward> loadObjects(String SQL, Object... params);

    public Integer getRowCount(String SQL, Object... params);

    public Ward queryForObject(String SQL, Object... params);

    public Integer getTotalRow();

    public Integer deleteObject(String id);

    public Boolean checkObjectExist(String id);

    public List<Ward> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters);

    public int filterObjectsSize(List<RequestFilter> filters);
    
    public Ward getByName(String name,int disitrict_id);
    
}
