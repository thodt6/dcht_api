/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unibro.ward;

import com.unibro.province.Province;
import com.unibro.utils.RequestFilter;
import com.unibro.utils.Global;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 *
 * @author THOND
 */
public class WardDAOImpl implements WardDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(WardDAOImpl.class);
    private String exceptionMsg = "";
    private HttpStatus exceptionCode = HttpStatus.OK;
    private String exceptionType = "";

    private static final String columns = "name,population,district_id";
    private static final String columns_key = "ward_id";

    @Autowired
    private static Environment env;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Ward createObject(final Ward object) {
        try {
            int size = columns.split(",").length;
            final String SQL = "INSERT INTO " + Global.getConfigValue("app.dbname") + ".ward(" + columns + ") VALUES (" + Global.getNumberCharater(size, "?") + ")";;
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int ret = jdbcTemplateObject.update((Connection connection) -> {
                try {
                    PreparedStatement ps = connection.prepareStatement(SQL, columns.split(","));
                    ps.setObject(1, object.getName());
                    ps.setObject(2, object.getPopulation());
                    ps.setObject(3, object.getDistrict_id());

                    return ps;
                } catch (SQLException ex) {
                    logger.error(ex);
                    exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                    exceptionMsg = ex.toString();
                    return null;
                }
            }, keyHolder);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                Long id = (Long) keyHolder.getKey();
                object.setWard_id(id.intValue());
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is created";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Ward getObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".ward WHERE " + Global.getMaskSQL(columns_key, "AND");
            Ward ret = this.jdbcTemplateObject.queryForObject(SQL, values, new WardMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Ward updateObject(final Ward object) {
        try {
            String[] prepared_arr = (columns + "," + columns_key).split(",");
            final String SQL = "UPDATE " + Global.getConfigValue("app.dbname") + ".ward SET " + Global.getMaskSQL(columns, ",") + " WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) {
                    try {
                        PreparedStatement ps = connection.prepareStatement(SQL, prepared_arr);
                        ps.setObject(1, object.getName());
                        ps.setObject(2, object.getPopulation());
                        ps.setObject(3, object.getDistrict_id());
// Primare key
                        ps.setObject(4, object.getWard_id());

                        return ps;
                    } catch (SQLException ex) {
                        logger.error(ex);
                        exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
                        exceptionMsg = ex.toString();
                        return null;
                    }
                }
            });
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return object;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is updated";
                return null;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Integer deleteObject(Ward object) {
        try {
            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".ward WHERE " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, object.getWard_id());
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer deleteObject(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            String SQL = "DELETE FROM " + Global.getConfigValue("app.dbname") + ".ward where " + Global.getMaskSQL(columns_key, "AND");
            int ret = jdbcTemplateObject.update(SQL, values);
            if (ret > 0) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "No object is deleted";
                return 0;
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public List<Ward> getAllObjects() {
        try {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".ward";
            List<Ward> objects = jdbcTemplateObject.query(SQL, new WardMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public List<Ward> loadObjects(String SQL, Object... params) {
        try {
            List<Ward> objects = jdbcTemplateObject.query(SQL, params, new WardMapper());
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return objects;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }

    }

    @Override
    public Ward queryForObject(String SQL, Object... params) {
        try {
            Ward ret = this.jdbcTemplateObject.queryForObject(SQL, params, new WardMapper());
            if (ret != null) {
                this.exceptionCode = HttpStatus.OK;
                this.exceptionMsg = "";
                return ret;
            } else {
                this.exceptionCode = HttpStatus.NOT_FOUND;
                this.exceptionMsg = "Object is not found";
                return null;
            }
        } catch (EmptyResultDataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.NOT_FOUND;
            this.exceptionMsg = "Object is not found";
            return null;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return null;
        }
    }

    @Override
    public Integer getRowCount(String SQL, Object... params) {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject(SQL, params, Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Integer getTotalRow() {
        try {
            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".ward", Integer.class);
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return 0;
        }
    }

    @Override
    public Boolean checkObjectExist(String id) {
        try {
            String[] keys_value = id.split("_");
            Integer v0 = Integer.valueOf(keys_value[0]);
            Object[] values = new Object[]{v0};

            this.exceptionCode = HttpStatus.OK;
            this.exceptionMsg = "";
            return jdbcTemplateObject.queryForObject("SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".ward WHERE " + Global.getMaskSQL(columns_key, "AND"), values, Integer.class) > 0;
        } catch (DataAccessException ex) {
            logger.error(ex);
            this.exceptionCode = HttpStatus.INTERNAL_SERVER_ERROR;
            this.exceptionMsg = ex.toString();
            return false;
        }
    }

    @Override
    public List<Ward> filterObjects(int first, int pageSize, String sortField, int sortOrder, List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".ward";
            //No sort
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            return this.loadObjects(SQL);
        } else {
            String SQL = "SELECT * FROM " + Global.getConfigValue("app.dbname") + ".ward WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            if (sortOrder == 0) {
                SQL += " ";
            }
            if (sortOrder == 1) {
                SQL += " ORDER BY " + sortField + " ASC";
            }
            if (sortOrder == -1) {
                SQL += " ORDER BY " + sortField + " DESC";
            }
            if (pageSize > 0) {
                SQL += " LIMIT " + first + "," + pageSize;
            }
            SQL = SQL.trim();
            logger.info("Filter SQL:" + SQL);
            return this.loadObjects(SQL, condition.toArray());
        }

    }

    @Override
    public int filterObjectsSize(List<RequestFilter> filters) {
        if (filters == null || filters.isEmpty()) {
            return this.getTotalRow();
        } else {
            String SQL = "SELECT count(*) FROM " + Global.getConfigValue("app.dbname") + ".ward WHERE ";
            List condition = new ArrayList();
            String filter_clause = "";
            for (RequestFilter filter : filters) {
                if (filter.getType().equals(RequestFilter.CONTAIN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    } else {
                        filter_clause += " OR UPPER(CAST(" + filter.getName() + " AS CHAR)) LIKE ? ";
                        condition.add("%" + filter.getValue() + "%");
                    }
                }
                if (filter.getType().equals(RequestFilter.EQUAL)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " = ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.GREATER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " >= ? ";
                        condition.add(filter.getValue());
                    }

                }
                if (filter.getType().equals(RequestFilter.LESS)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <= ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.IN)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " IN(" + filter.getValue() + ")";
                    } else {
                        filter_clause += " OR " + filter.getName() + " IN(" + filter.getValue() + ")";
                    }
                }
                if (filter.getType().equals(RequestFilter.BIGGER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " > ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.SMALLER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " < ? ";
                        condition.add(filter.getValue());
                    }
                }
                if (filter.getType().equals(RequestFilter.DIFFER)) {
                    if (filter.getRequired()) {
                        filter_clause += "AND " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    } else {
                        filter_clause += " OR " + filter.getName() + " <> ? ";
                        condition.add(filter.getValue());
                    }
                }
            }
            if (filter_clause.length() > 0) {
                filter_clause = filter_clause.substring(4).trim();
            }
            SQL = SQL + filter_clause;
            logger.info("Filter SQL:" + SQL);
            return this.getRowCount(SQL, condition.toArray());
        }

    }

    /**
     * @return the exceptionMsg
     */
    public String getExceptionMsg() {
        return exceptionMsg;
    }

    /**
     * @param exceptionMsg the exceptionMsg to set
     */
    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    /**
     * @return the exceptionCode
     */
    public HttpStatus getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(HttpStatus exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    /**
     * @return the exceptionType
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * @param exceptionType the exceptionType to set
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }
    
    @Override
    public Ward getByName(String name, int district_id){
        return this.queryForObject("SELECT * FROM ward WHERE UPPER(name)=? AND district_id=?", name.toUpperCase(),district_id);
    }
    
}
