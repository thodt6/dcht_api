package com.unibro.ward;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import com.unibro.utils.Global;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.math.BigDecimal;

public class Ward implements Serializable {

    private Integer ward_id = 0;
    private String name = "";
    private Integer population = 0;
    private Integer district_id = 0;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Ward.class);

    public void setWard_id(Integer ward_id) {
        this.ward_id = ward_id;
    }

    @JsonProperty("ward_id")
    public Integer getWard_id() {
        return this.ward_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    @JsonProperty("population")
    public Integer getPopulation() {
        return this.population;
    }

    public void setDistrict_id(Integer district_id) {
        this.district_id = district_id;
    }

    @JsonProperty("district_id")
    public Integer getDistrict_id() {
        return this.district_id;
    }

    public static Ward getInstant() {
        Ward instant = new Ward();
        instant.setWard_id(0);
        instant.setName("");
        instant.setPopulation(0);
        instant.setDistrict_id(0);

        return instant;
    }

    public Ward() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Ward)) {
            return false;
        }
        Ward compareObj = (Ward) obj;
        return (compareObj.getUniqueKey().equals(this.getUniqueKey()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.getUniqueKey() != null ? this.getUniqueKey().hashCode() : 0);
        return hash;
    }

    public HashMap toHashMap() {
        HashMap map = new HashMap();
        map.put("ward_id", this.ward_id);
        map.put("name", this.name);
        map.put("population", this.population);
        map.put("district_id", this.district_id);

        return map;
    }

    public String getUniqueKey() {
        return ward_id.toString();
    }

}
